@extends('layout.master')

@section('content')
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Edit BUKU</h4>
            </div>
            <div class="panel-body">
                <form action="{{url('update', $data->id)}}" method="post">
                    <div class="form-group row">
                        <label for="title" class="col-sm-2 col-form-label">TITLE</label>
                        <div class="col-sm-10">
                            <input type="text" name="title" id="title" value="{{$data->nim}}" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="writer" class="col-sm-2 col-form-label">Writer</label>
                        <div class="col-sm-10">
                            <input type="text" name="writer" id="writer" value="{{$data->writer}}" class="form-control">
                        </div>
                    </div>                    
                    <div class="form-group row">
                        <label for="publisher" class="col-sm-2 col-form-label">Publisher</label>
                        <div class="col-sm-10">
                            <select class="custom-select form-control" name="publisher">
                            <option value="ABCD">ABCD</option>
                              <option value="EFGH">EFGH</option>
                              <option value="IJKLM">IJKLM</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="send" id="send" value="Simpan" class="btn btn-success">{!!csrf_field()!!}                       
                    </div>
                </form>
            </div>
        </div>
@endsection
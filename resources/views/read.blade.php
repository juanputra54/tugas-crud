@extends('layout.master')

@section('content')
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Detail BUKU</h4>
            </div>
            <div class="panel-body">
                    <div class="form-group row">
                        <label for="title" class="col-sm-2 col-form-label">Title</label>
                        <div class="col-sm-10">
                            <input type="text" name="title" id="title" value="{{$data->title}}" class="form-control" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="writer" class="col-sm-2 col-form-label">Writer</label>
                        <div class="col-sm-10">
                            <input type="text" name="writer" id="writer" value="{{$data->writer}}" class="form-control" readonly>
                        </div>
                    </div>                    
                    <div class="form-group row">    
                        <label for="publisher" class="col-sm-2 col-form-label">Publisher</label> 
                        <div class="col-sm-10">   
                            <input type="text" name="publisher" id="publisher" value="{{$data->publisher}}" class="form-control" readonly>
                        </div>
                    </div>
                    <form action="{{url('back')}}" method="get">
                        <div class="form-group">
                            <input type="submit" value="Kembali" class="btn btn-success">
                        </div>
                    </form>
            </div>
        </div>
@endsection
@extends('layout.master')

@section('content')
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Tambah Data BUKU</h4>
            </div>
            <div class="panel-body">
                <form action="{{url('insert')}}" method="post">
                    <div class="form-group row">
                        <label for="title" class="col-sm-2 col-form-label">TITLE</label>
                        <div class="col-sm-10">
                            <input type="text" name="title" id="title" class="form-control" required="require">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="writer" class="col-sm-2 col-form-label">Writer</label>
                        <div class="col-sm-10">
                            <input type="text" name="writer" id="writer" class="form-control">
                        </div>
                    </div>                    
                    <div class="form-group row">
                        <label for="publisher" class="col-sm-2 col-form-label">Publisher</label>
                        <div class="col-sm-10">
                            <select name="publisher" class="custom_select form-control" id="publisher">
                              <option value="ABCD">ABCD</option>
                              <option value="EFGH">EFGH</option>
                              <option value="IJKLM">IJKLM</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="send" id="send" value="Simpan" class="btn btn-success">{!!csrf_field()!!}                       
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
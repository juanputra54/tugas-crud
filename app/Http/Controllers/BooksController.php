<?php

namespace App\Http\Controllers;
use DB;
use App\Books;
use Illuminate\Http\Request;

class BooksController extends Controller
{
    public function index(){
        $data=Books::all();
           return view('index',compact('data'));
       }
   
       public function back(){
        return redirect ('/');
       }
       
       public function create(){
        return view('create');
       }
   
       public function insert(Request $request){
        $data=new Books();
           $data->title=$request->get('title');
           $data->writer=$request->get('writer');
           $data->publisher=$request->get('publisher');
           $data->save();
        return redirect ('/');
       }
   
       public function delete($id){
        $data=Books::find($id);
           $data->delete();
           return back();
       }
   
       public function edit($id){
        $data=Books::find($id);
        return view('edit',compact('data'));
       }
   
       public function update(Request $request, $id){     
        $data = Books::findOrFail($id);
        $data->title=$request->get('title');
        $data->writer=$request->get('writer');
        $data->publisher=$request->get('publisher');
        $data->save();
        return redirect ('/')->with('alert-success','Data berhasil Diubah.');
       }
   
       public function read($id){
        $data=Books::find($id);
        return view('read',compact('data'));
       }
    }